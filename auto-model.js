var Nightmare = require('nightmare')
var vo = require('vo')
var read = require('read')
var fs = require('fs')

var words = fs.readFileSync('words.txt', 'utf8').split("\r\n")
var ALPHABET = "abcdefghijklmnopqrstuvwxyz".split("")
var WKSPACENAME = "Beyond"

/*
I wrote this as a one-off script, starting with the NightmareJS library layer
on top of the Selenium Webdriver API.  I realized later that NightmareJS doesn't
have support for uploading files, so this is a separate script where the bot
directs the user to manually upload the files.

If I was doing this again or wanted to run this bot sustainably for a long time
with no user input, I'd do it in Python.  The Selenium Webdriver API in python allows
for file uploads.

But then again, if I was going to want to run the bot without user input for a
long time, I'd also add detection features that determine what phase of process
a given model is in.

Anyway, this method was the quickest to win the contest, since Nightmare is easier
to use than Selenium.  It wouldn't be difficult to translate into python now.
*/

read({ prompt: "Username: "}, function (er, username) {
  read({ prompt: 'Password: ', silent: true }, function(er, password) {
    bot(username, password, process.argv[2]);
  })
})

function sample (lst) {
  /* Returns a random sample of the list, with equal weight to each element. */
  return lst[Math.floor(Math.random () * lst.length)]
}

function randomLetters(n, s) {
  if (n == 0) return s
  return randomLetters(n - 1, s + sample(ALPHABET))
}


function bot (user, pass) {
  vo(function* () {
    var nightmare = Nightmare({ show: true })
    var signIn = yield nightmare
      .goto('https://platform.purepredictive.com')
      .wait(1000)
      .type('#userName', user)
      .type('input[type=password]', pass)
      .evaluate(function () {
        $("body").find("button:contains('Sign in')")[0].click()
      })

    while (true) {
      var initModel = yield nightmare
        .wait(500)
        .wait("#addNewProgramButton")
        .click("#addNewProgramButton")
        .wait(800)
        .evaluate(function () {
          $("input[name=programWorkspace]").val("")
        })
        .type("input[name=programWorkspace]", WKSPACENAME)
        .wait(400)
        .type("input[name=programName]", sample(words)+ "_" + sample(words))
        .type("input[name=programDescription]", randomLetters(Math.floor((Math.random() * 4)) + 3, ""))
        .click("#btn_createProgramButton")
        .wait(2000)
        //.wait("#btn_addFile")
        .click("#btn_addFile")
        .wait(2000)
        //.wait("#fileToUpload")
        .click("#fileToUpload")

        /* That's right, the user has exactly 6 seconds to upload the file. */
        .wait(6000)
        .evaluate(function () {
          $("span[class=xn-text]")[1].click()
        })
        .wait(1000)

      /* Why delete the first dataset?  Because then 
      the human user (me) need only focus on the first file. */

      ls = fs.readdirSync("datasets")
      fs.unlinkSync("datasets/" + ls[1])
    }

      yield nightmare.end()
      return link
  })(function (err, result) {
    if (err) return console.log(err)
  })
}
