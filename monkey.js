var Nightmare = require('nightmare');
var vo = require('vo');
var read = require('read')

/* Since I can't predict when exploring data/training will be done,
the bot has several different modes that can be run by the user.  These are

data - selects the target feature (top one on the list)
train - starts training the model
deploy - deploys the trained model.

The bot takes as input a page number and a starting "index" (row # on the website, 
starting with 0).  It will complete the task for every entry past the row #, inclusive.

The bot is done when it is looking for the next row but can't find it.  If this is the
case, it sits and waits indefinitely.  I left this as the case because I wanted to be
able to look at the results in the window.
*/

if (process.argv.length <= 3 || ["data", "train", "deploy"].indexOf(process.argv[2]) == -1) {
  console.log("Usage: node monkey.js <data|train|predict> <page_#> <start_index>");
  process.exit(1);
}

var MODE = process.argv[2]
var PAGENUM = process.argv[3]
var STARTNUM = process.argv[4]
var PAGENUM_ID = {
  "data"   : "5",
  "train"  : "5",
  "deploy" : "3"
}[MODE]
var BOT_COMMAND = {
  "data"   : mode_data,
  "train"  : mode_train,
  "deploy" : mode_deploy
}[MODE]
var oo = 2000 // "infinity"

read({ prompt: "Username: "}, function (er, username) {
  read({ prompt: 'Password: ', silent: true }, function(er, password) {
    monkey(username, password, process.argv[2]);
  })
})

/* These functions describe the various modes of the bot. */
function mode_data (nightmare, i) {
  var dataInsertion = nightmare
    .wait("#seeDetailsButton_" + PAGENUM_ID + "_" + i)
    .click("#seeDetailsButton_" + PAGENUM_ID + "_" + i)
    .wait("#subjectObsUploaded") // Wait for "Add label" buttons to show up
    .wait(3000)
    .click("#btn_label")

    .wait(2000)
    .evaluate(function () {
      $("a[data-toggle=modal]")[2].click()
    })
    .wait(2000)
    .evaluate(function () {
      $("button[data-dismiss=modal]")[10].click()
    })
    .wait(2000)
    .evaluate(function () {
      $("span[class=xn-text]")[1].click()
    })
    .wait(2000)
}

function mode_train (nightmare, i) {
  nightmare
    .wait("#seeDetailsButton_" + PAGENUM_ID + "_" + i)
    .click("#seeDetailsButton_" + PAGENUM_ID + "_" + i)
    .wait(800)
    .click("#btn_train")
    .wait(800)
    .evaluate(function () { 
      $("body").find("button:contains('Train')")[1].click()
    })
    .wait(4000)
    .evaluate(function () {
      $("span[class=xn-text]")[1].click()
    })
    .wait(3000)
}

function mode_deploy (nightmare, i) {
  nightmare
    .wait("#seeDetailsButton_" + PAGENUM_ID + "_" + i)
    .click("#seeDetailsButton_" + PAGENUM_ID + "_" + i)
    .wait(10800)
    .click("#btn_selectModel")
    .wait(3000)
    .click("#btn_deploy")
    .wait(1000)
    .evaluate(function () {
      $("span[class=xn-text]")[1].click()
    })
    .wait(1500)
}

function monkey (user, pass) {
  vo(function* () {
    var nightmare = Nightmare({ show: true });
    
    yield nightmare
      .goto('https://platform.purepredictive.com')
      .wait()
      .type('#userName', user)
      .type('input[type=password]', pass)
      .evaluate(function () {
        $("body").find("button:contains('Sign in')")[0].click()
      })
      .wait("#predict_1_0")

    for (var i = STARTNUM; i < oo; i++) {
      /* 
      Pages beyond 10 aren't visible from page 1.
      Navigate to page 10 to show them, then continue on
      to desired page.
      */
      if (PAGENUM > 10) {
        yield nightmare
          .evaluate(function (pg) {
            $("body").find("a:contains('" + pg + "')")[0].click()
          }, 10)
          .wait(1400)
      }

      yield nightmare
        .evaluate(function (pg) {
          $("body").find("a:contains('" + pg + "')")[0].click()
        }, PAGENUM)
      BOT_COMMAND(nightmare, i);
    }

      yield nightmare.end();
      return link;
  })(function (err, result) {
    if (err) return console.log(err);
    console.log(result);
  });
}