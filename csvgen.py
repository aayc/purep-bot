import csv;
import random as rng;

NUM_CSV = 2
FILE_NAME_LENGTH = 3
MIN_COLS = 20
MAX_COLS = 30
MAX_VAL = 2098
MIN_VAL = 2
MAX_ROWS = 10000
MIN_ROWS = 400
QUIET = True

# Running python csvgen.py will generate NUM_CSV datasets with absolutely random data,
# and a random number of rows between MIN_ROWS and MAX_ROWS, random number of cols
# between MIN_COLS and MAX_COLS, with numbers having values between MIN_VAL, MAX_VAL.

# Each column will be a random word, except for the target feature (which is binary),
# and always named "goal"

# Each dataset will have a name consisting of FILE_NAME_LENGTH words joined by "_"

with open("words.txt") as f:
	words = f.readlines()
words = map(lambda x: x.strip(), words);

def genDataset(fname):
	used = set();
	def getWord():
		w = rng.choice(words)
		if w in used:
			return getWord()
		else:
			used.add(w)
			return w

	numCols = range(rng.randint(MIN_COLS, MAX_COLS))
	cols = map(lambda x: getWord(), numCols)
	data = [];
	data.append(map(lambda x: x, cols))
	data[0].append("goal")

	numRows = rng.randint(MIN_ROWS, MAX_ROWS)
	for i in range(0, numRows):
		row = map(lambda x: round(rng.random() * rng.randint(MIN_VAL, MAX_VAL), rng.randint(1, 3)), cols)
		row.append(rng.randint(0, 1))
		data.append(row);

	fl = open("datasets/" + fname + '.csv', 'w')

	writer = csv.writer(fl)
	for row in data:
	    writer.writerow(row)
	fl.close() 

for i in range(0, NUM_CSV):
	datasetName = "_".join([rng.choice(words) for j in range(0, FILE_NAME_LENGTH)])
	if not QUIET: 
		print "Generating random dataset named:", datasetName
	genDataset(datasetName)