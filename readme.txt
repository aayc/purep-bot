Author: Aaron Y. Chan

I built this bot for the PurePredictive SuperModeler competition, for reasons:
	1. I had just heard about a JS bot framework, NightmareJS
	2. It was Tuesday and I had no classes.

The bot works in 5 phases:
	0.  The preload phase run by "python csvgen.py" which creates a bunch of random datasets and puts them in datasets/
	1.  The "generation" phase run by auto-model.js, which has the user manually input dataset files.  I couldn't figure out how to use NightmareJS to upload files.  I know the Selenium WebDriver API can do it, in Python.  If I was to write a more long-term bot I'd port the code to Python.  More information in the remarks in auto-model.js
	2.  The "target feature selection" phase run by "monkey.js data" command, which selects the "goal" target feature for each data set.  This should be run after the data has been explored.
	3.  The "trainer" phase run by "monkey.js train" command, which clicks "Train" in all ready models.
	4.  The "deploy" phase which selects the best performing model and deploys it.

Development time: about 2 hours
Needs improvement: auto-file upload, and detecting automatically which phase each model is in.  This would require being ported to python
Gotchas: timing the bot is tedious, but waiting on certain elements to appear is unpredictable.  
